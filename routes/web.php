<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::resource('/listas','ListaController');
Route::post('/listas/{id}/elementos', 'ListaController@addElemento');
Route::get('/listas/{id}/ListaFavorita', 'ListaController@ListaFavorita');
Route::get('/elementos/{id}/hacer', 'ListaController@hacer');
Route::get('/elementos/{id}/deshacer', 'ListaController@deshacer');
Route::get('/elementos/{id}/borrar', 'ListaController@borrar');
//shows
Route::get('listas/{id}', function ($id) {
   return "Ver contenido de lista $id";
});


Route::get('/home', 'HomeController@index')->name('home');
