@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Inicio</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                        Estás registrado!
                        <div>
                                <a href="/listas" class="btn btn-primary">Ver mis listas</a>
                        </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
