@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Mis listas</div>

                <div class="card-body">
                 <form class="form"  method="post" action="/listas">
                    {{ csrf_field() }}

                    <div class="form-group">
                        <label>Nombre</label>
                        <input class="form-control" type="text" name="nombre" value="{{old('nombre')}}">

                        @if ($errors->first('nombre'))
                        <div class="alert alert-danger ">
                            {{$errors->first('nombre')}}
                        </div>
                        @endif

                    </div>

                    <input type="submit" value="Nueva lista" class="btn btn-success"  role="button">
                </form>

            </div>

            <div class="card-body">
              <a  href="/listas" class="btn btn-success"  role="button" >Volver a listas</a></td>
          </div>


      </div>
  </div>
</div>
</div>
@endsection
