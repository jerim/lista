@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-10">
      <div class="card">
        <div class="card-header">Mis listas <br>
           @if(Session::has('lista'))

             Mi lista favorita es : {{Session::get('lista')->nombre}}

           @endif

        </div>

        <div class="card-body" >
          <table class="table">
            <tr>
              <th>Lista</th>
              <th>Usuario</th>
              <th>Acciones</th>
            </tr>
            @forelse($listas as $lista)
          {{--  @can('index',$lista) --}}
            <tr>
              <td>
                {{$lista->nombre}}
              </td>
              <td>{{$lista->user->name}}</td>
              <td>
               <form method="post" action="/listas/{{$lista->id}}">
                 {{ csrf_field() }}
                 <a  href="/listas/{{$lista->id}}" class="btn btn-success"  role="button" >Ver</a>
                 <a  href="/listas/{{$lista->id}}/edit" class="btn btn-success"  role="button" >editar</a>
                 <a  href="/listas/{{$lista->id}}/ListaFavorita" class="btn btn-success"  role="button" >Lista Favorita</a>
                 <input type="hidden" name="_method" value="delete">
                 <input type="submit" value="Destroy" class="btn btn-danger"  role="button">
               </form>

             </td>
           </td>
         </tr>

        {{-- <td>NO eres este usuario</td>
         @endcan --}}
         @empty
         No existe ninguna lista creada

         @endforelse

       </table>

     </div>

     <div class="card-body">
      <a  href="/listas/create" class="btn btn-success"  role="button" >Crear Listas</a>
    </div>


  </div>
</div>
</div>
</div>
@endsection
