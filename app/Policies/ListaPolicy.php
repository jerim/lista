<?php

namespace App\Policies;

use App\User;
use App\Lista;
use Illuminate\Auth\Access\HandlesAuthorization;

class ListaPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function index(User $user) {
/*        if($user->id ==1){
            return true;
        }else{
            return $user->id == $lista->user_id;
        }
*/    }

    public function view(User $user, Lista $lista){

        return $user->id == 1 || $user->id == $lista->user_id;
        // return $user->id == 1  ? true : $user->id == $lista->user_id;

        /*if($user->id ==1){
            return true;
        }else{
            return $user->id == $lista->user_id;
        }*/
    }

    public function manage(User $user) {
        return $user->id == 1;
    }
}
