<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lista extends Model
{
     protected $fillable =['nombre','user_id'];

     public function elementos(){

        return $this->hasMany(Elemento::class);
     }

     public function user(){
        return  $this->belongsTo(User::class);
     }
}
