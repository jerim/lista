<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Lista;
use App\Elemento;

class ListaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Lista::with('elementos','user')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
            'nombre' => 'required|min:4|max:15|unique:listas,nombre',
            'user_id' =>' exists:users,id',
        ];
        $messages = [
            'required' => 'Falta por escribir el nombre',
            'unique'=>'El nombre debe ser unico',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 15 caracteres'
        ];

        $validator=Validator::make($request->all(),$rules,$messages);

        if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $lista= new Lista;
        $lista->fill($request->all());
        $lista->save();

        return $lista;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lista=Lista::with('elementos')->find($id);
        if($lista){
            return $lista;
        }else{
            return response()->json([
                'message'=>"Lista no encontrada",
            ],400);
        }

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $rules=[
            'nombre' => 'required|min:4|max:15|unique:listas,nombre',
            'user_id' =>' exists:users,id',
        ];
        $messages = [
            'required' => 'Falta por escribir el nombre',
            'unique'=>'El nombre debe ser unico',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 15 caracteres'
        ];

        $validator=Validator::make($request->all(),$rules,$messages);

        if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

        $lista=Lista::with('elementos','user')->find($id);

        if(!$lista){
            return response()->json([
                 'message'=>"Lista no actualizada ni encontrada",
            ],404);
        }

        $lista->fill($request->all());
        $lista->save();
        $lista->refresh();

        return $lista;


    }

    public function addElemento(Request $request,$id){
         $rules=[
           'texto' => 'required|min:4|max:15',

          ];

          $messages = [
            'required' => 'Falta por escribir el texto',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 15 caracteres'
          ];

          $validator=Validator::make($request->all(),$rules,$messages);

          if($validator->fails()) {
            return response()->json($validator->errors(),400);
        }

         $elemento= new Elemento;

         $elemento->texto = $request->texto;
         $elemento->lista_id = $id;
         $elemento->save();

         $lista= Lista::with('elementos')->find($id);
         return $lista;

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Lista::destroy($id);
        return response()->json([
            'message'=>'Lista destruida',
        ],201);
    }
}
