<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Lista;
use App\Elemento;
use App\User;
use Session;

class ListaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct(){
        $this->middleware('auth');
    }

    public function index()
    {
        $user = \Auth::user();
        if($user->can('manage',Lista::class)){
            $listas= Lista::all();

        }else{
            $listas = Lista::where('user_id',$user->id)->get();
        }

        //return $listas;
        return view('listas.index',['listas'=>$listas]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $listas= Lista::all();
        return view('listas.create',['listas'=>$listas]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules=[
        'nombre' => 'required|min:4|max:15|unique:listas,nombre',
        'user_id' =>' exists:users,id',
         ];
        $messages = [
            'required' => 'Falta por escribir el nombre',
            'unique'=>'El nombre debe ser unico',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 15 caracteres'
        ];

        $request->validate($rules,$messages);


        $lista = new Lista;
        $lista->fill($request->all());
        $lista->user_id = \Auth::user()->id;
        $lista->save();

        return redirect('/listas');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $lista=Lista::find($id);
        $this->authorize('view', $lista);
        return view('listas.show',['lista'=>$lista]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function addElemento(Request $request,$id){

        $rules=[
        'texto' => 'required|min:4|max:15',

         ];
        $messages = [
            'required' => 'Falta por escribir el texto',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 15 caracteres'
        ];

        $request->validate($rules,$messages);

        $elemento = new Elemento;
        $elemento->texto = $request->texto;
        $elemento->lista_id = $id;
        $elemento->save();

        return back();

    }

     public function ListaFavorita($id,Request $request){
        $lista=Lista::find($id);
        $request->session()->put('lista',$lista);

        return back();
     }


    public function hacer($id){
      $elemento= Elemento::find($id);
      $elemento->hecho=false;
      $elemento->save();

      return back();

    }

    public function deshacer($id){
     $elemento= Elemento::find($id);
      $elemento->hecho=true;
      $elemento->save();

      return back();

    }

    public function borrar($id){
      $elemento=Elemento::destroy($id);
      return back();
    }


    public function edit($id)
    {
        $lista=Lista::find($id);
        return view('listas.edit',['lista'=>$lista]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
         $rules=[
        'nombre' => 'required|min:4|max:15|unique:listas,nombre,'.$id,
        'user_id' =>' exists:users,id',
         ];
        $messages = [
            'required' => 'Falta por escribir el nombre',
            'unique'=>'El nombre debe ser unico',
            'min'=> 'minimo tiene que tener 4 caracteres',
            'max'=>'no puede tener más de 15 caracteres'
        ];

        $request->validate($rules,$messages);


        $lista = Lista::find($id);
        $lista->fill($request->all());
        $lista->user_id = \Auth::user()->id;
        $lista->save();

        return redirect('/listas');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $lista=Lista::destroy($id);
         return back();

    }
}
